// console.log("hello from js file")

//target form componenet and placement it inside a variable
let registerForm = document.querySelector("#registerUser");

// inside the first parameter of the method, describe event that it will listen to. 2nd parameter let's describe the action or the procedure that will happen upon trigerring the said event.
registerForm.addEventListener("submit", (event) => {
	//to avaoid page refresh/redirection once the event is triggered
	event.preventDefault();		

	//capture values inside the input fields. then assign them to a new variable
	let firstName = document.querySelector("#firstName").value
	// create a checker to make sure we capture the values
	// console.log(firstName)
	let lastName = document.querySelector("#lastName").value
	// create a checker to make sure we capture the values
	// console.log(lastName)
	let userEmail = document.querySelector("#userEmail").value
	// create a checker to make sure we capture the values
	// console.log(userEmail)
	let mobileNo = document.querySelector("#mobileNo").value
	// create a checker to make sure we capture the values
	// console.log(mobileNo)
	let password = document.querySelector("#password1").value
	// create a checker to make sure we capture the values
	// console.log(password)
	let verifyPassword=document.querySelector("#password2").value
	//create a checker to make sure we capture the values
	// console.log(verifyPassword)


	// data validation for register page to check and verify that the data is accurate and prevent multiple entries (storage space is properly utilized)
	//control structure to determine the next set of procedures before the user can register a new account.
	//STRETCH GOALS: 
	// 1) The password should contain both alphanumeric and 1 special character.
	// 2) Find a way so that the mobile number will be able to accept both foreign and local mobile number formats.
		
		if((firstName !== "" && lastName !== "" && userEmail !== "" && password !== "" && verifyPassword !== "" && mobileNo.length === 11)) {
			
			if (password.length < 8) {
				alert('Password is less than 8 characters!')
				return false
			};
			
			// if (!password.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]$/)) {
			// 	alert('Password must be alphanumeric with at least one special character')
			// 	console.log(password)
			// 	return false
			// };

			if (password !== verifyPassword) {
				alert('Password does not match')
				return false
			}
		//before we allow a user to create a new acct, we must check if the email he inputted does not exist yet in teh database. 
		//new request to integrate email-exists method.
		//upon sending the request, we re instatiating apromise object
			fetch('https://protected-garden-34848.herokuapp.com/api/users/email-exists', {
				method: 'POST',
				headers: {
					'Content-Type': 'Application/json'
				},
				body:JSON.stringify({
					email: userEmail
				})
			}).then(res => {
					return res.json()
			}).then(email => {
				if (email === false) {			//=== false: refer to user controller logic from email-exists.
			//allow the user to register
			//create a new acct via FETCH REQUEST for the user using the data that he entered
			//params: (1) URL - destination of the request, (2)	[options] - optional parameters: method, headers, etc.
					fetch('https://protected-garden-34848.herokuapp.com/api/users/register', {
					//describe the structure of request for regiter via an object
					method: 'POST',
					headers: {
						'Content-Type': 'Application/json'
					},
					//API only accepts request in a string format, thus JSON in Postman {"fName": "shara"}
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: userEmail,
						mobileNo: mobileNo,
						password: password
					})
							//promise
				}).then(res => {
							// console.log(res)
							return res.json()
							// console.log(res)
				}).then(data => {
					console.log(data)
							// create controll structure for proper response depending on the return from the backend
					if(data === true){
						Swal.fire("New account registered successfully!")
						window.location.replace("./login.html")
					}else{
						Swal.fire("Something went wrong during processing!")
						}
					}
					)
			} else {
				Swal.fire("The email already exists!")
			}
		}
		)
	} else {
		Swal.fire("Oh no!!!")
   }
}) 

		