// captured navsession element from teh navbar element
let navItem = document.querySelector("#navSession")

//check if there's someone logged-in
let userToken = localStorage.getItem('token')
//create a control structure that will determin teh display inside a navbar if there is a user currently logged-in
if (!userToken) {
	navItem.innerHTML = `
		<li class="nav-item">
			<a href="./login.html" class="nav-link">Login</a>
		</li>
	`
} else {
	navItem.innerHTML = `
		<li class="nav-item">
			<a href="./logout.html" class="nav-link">Logout</a>
		</li>
	`
}
