//Identify which course this page needs to display
//get the key-value pair passed in the url by using URLSearchParams(). This method returns an object.
//window.location - returns a location object with information about the "current" location of the document.
// .serch() --> contains the query string section of the current url.
let urlValues = new URLSearchParams(window.location.search)
// console.log(urlValues) //checker
let courseid = urlValues.get('courseId')
// console.log(id) //checker

//capture the value of the access token inside the local storage

let token = localStorage.getItem('token');
// console.log(token);

//set a container for all the details that will be displayed
let courseName = document.querySelector("#courseName");
let coursePrice = document.querySelector("#coursePrice");
let courseDesc = document.querySelector("#courseDesc");
let enroll = document.querySelector("#enrollmentContainer");

//send a request to the endpoint that will let the course details be displayed.
fetch(`https://protected-garden-34848.herokuapp.com/api/courses/${courseid}`)
.then(res => res.json())
.then(data => {
	// console.log(data)

	courseName.innerHTML = data.name
	coursePrice.innerHTML = data.price
	courseDesc.innerHTML = data.description
	enroll.innerHTML = `<a id="enrollButton" class="btn btn-success text-white btn-block">Enroll</a>`

//user clicks on the enroll button
document.querySelector('#enrollButton').addEventListener('click', () => {
		// alert("Successfully enrolled!")
				//insert the course inside the enrollments array of the user
				fetch('https://protected-garden-34848.herokuapp.com/api/users/enroll', {
					method: 'POST',
					headers: {
						'Content-type': 'Application/json',
						'Authorization' : `Bearer ${token}`
					},
					body: JSON.stringify ({
						courseId: courseid
					})
				})
				.then(res => {
					return res.json()
				}).then (data => {
					console.log(data)
					//control structure that will determine a response accdg to the result, to displayed to the client
					if (data === true) {
						alert("Thank you for enrolling")
						window.location.replace("./courses.html")
					} else {
						alert('Pls try again')
					}
				})
			})
		})
