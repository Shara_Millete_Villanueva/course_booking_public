//target the container from html document
let admin = document.querySelector('#adminButton')
let courseDetails = document.querySelector('#coursesContainer');

//determin e if there's a user currently logged-in. capture one of the properties that is currently stored in our web storage
const isAdmin = localStorage.getItem("isAdmin")
//create a controll structure to determine the display in the front end
if (isAdmin == "false" || !isAdmin)
{
	admin.innerHTML = null;
} else {
	admin.innerHTML = 
	`
	<div class="col-md-2 offset-md-10">
		<a href="./../../public/pages/addCourse.html" class="btn btn-block btn-warning"> Add Course</a>
	</div>
	`
}


//send a request to retrieve all documents from the courses collection
fetch('https://protected-garden-34848.herokuapp.com/api/courses').then(res => res.json()).then(data => {console.log(data)

//instantiate variable that will display the result in the browser depending on the return.
let courseData;

//create a control structure that will determine the value of the variable will hold
if (data.length < 1) {
	courseData = "No courses available"
	console.log('no courses available')
	courseDetails.innerHTML = courseData
} else {
	// iterate the courses collection and display each course on the page
	courseData = data.map(course => {
		//check the structure of each element inside the courses collection
		console.log(course._id)
		console.log(course.name)
		//use template literals to inject the properties of our object inside each section of a card component
		//fix the current bahavior of courses page - display all teh courses
		 let courseAdminControls = ""
		 if (isAdmin == "false" || !isAdmin) {
		 	courseAdminControls = `
		 		<a href="./course.html?courseId=${course._id}" class="btn btn-success text-white btn-block">View Course Details</a>
		 		`
		 } else {
		 	courseAdminControls = `
				<a href="#" class="btn btn-danger text-white btn-block">Delete</a>
				<a href="./course.html" class="btn btn-primary text-white btn-block">Edit</a>
				`
		 }
		return(
			`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h3 class="card-title">Course Name: ${course.name}</h3>
							<p class="card-text text-left">Description: ${course.description}</p>
							<p class="card-text text-left">Course Price: ${course.price}</p>
							<p class="card-text text-left">Created On: ${course.createdOn}</p>
						</div>
						<div class="card-footer">
							${courseAdminControls}
						</div>
					</div>
				</div>
			`)	
	}).join("") 	//to create a return of a new string; to concatenate/combine the elements of the array into 1 string data type
		courseDetails.innerHTML = courseData;
	}

	})
