	
//target our form component 
let logInForm = document.querySelector('#loginUser');
//logInForm will be used by client to input her account to be authenticated by the app

logInForm.addEventListener('submit', (event) => {
	event.preventDefault();

//capture the values of form components.
	let userEmail = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value
	// console.log(userEmail);
	// console.log(password);
//validate data inside of the input value
if (userEmail == "" || password == "") {		//OR so that alert will flash if none or only one has input
	alert('Please input email and/or password')
} else {
		// send request to the endpoint
		fetch("https://protected-garden-34848.herokuapp.com/api/users/login", {
			method: 'POST',
			headers: {
				'content-type': 'application/json'
			},
			body: JSON.stringify({
				email: userEmail,
				password: password,
			})
		}).then(res => {
			return res.json()
		}).then(input => {
			console.log(input);
			// save the generated access token inside the local storage property of the browser
			if (input.accessToken) {
				localStorage.setItem('token', input.accessToken)

				// alert('successfully generated access token!');

				//before redirecting, you have to identify the access rights to be granted to the user (isAdmin)
				fetch('https://protected-garden-34848.herokuapp.com/api/users/details', {
					method: 'GET',
					headers: {'Authorization':`Bearer ${input.accessToken}`
					}
				}).then (res => {
					return res.json()
				}).then (data => {
					console.log(data)
					//save the 'id', 'isAdmin' properties
				localStorage.setItem('id',data._id)
				localStorage.setItem('isAdmin',data.isAdmin)

				if (data.isAdmin == true) {
					window.location.replace('./courses.html')
				} else {
					window.location.replace('./profile.html')
				}	
				// redirect user to another page
				// window.location.replace('./profile.html')
				})

			} else {
				//if no acess toen was generated
				alert('No aceess token generated')
			}

		})	
	}
})
