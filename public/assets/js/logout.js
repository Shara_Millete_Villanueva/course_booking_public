// console.log('logout.js')

///upon logging-out, the credentials of the user must be removed from the local storage
//hot to delete these
localStorage.clear();

//return to log-in page
window.location.replace("./login.html")