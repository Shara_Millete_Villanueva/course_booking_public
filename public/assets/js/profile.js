//get the value of the access token inside the local storage and assign a variable to it
let token = localStorage.getItem('token')
console.log(token)

let userProfile = document.querySelector('#profileContainer')


//display the info about the user who logged in by fetch method
fetch('https://protected-garden-34848.herokuapp.com/api/users/details',
	{
		method: 'GET',
		headers: {
			'Content-type': 'Application/json',
			'Authorization': `Bearer ${token}`
		}
	})
.then(res => res.json())
.then(data => {
		// console.log(data)
		// display the info in the front end? target the div element  using its id attribute
		//create a section to display user's enrollments
		//get all the elements inside the enrollments array.

		// 	console.table(data.enrollments)
		// 	let userSubjects = data.enrollments.map(subject => {
		// 		console.log(subject)
		// fetch(`http://localhost:3000/api/courses/${subject.courseId}`).then(res => res.json()).then(data => {
		// 	// 	console.log (subject.Description)


		// 	return (
		// 		//create a container that will display the properties of the object of the subject that i want to enroll to

		// 		`

		// 					<td> ${subject.enrolledOn}</td>
		// 					<td> ${subject.status}</td>

		// 			`
		// 	)
		// }).join("") // this will remove the unnecessary commas
		// console.log(userSubjects)
		// =====================================================================
		data.enrollments.forEach(subject => {
			fetch(`https://protected-garden-34848.herokuapp.com/api/courses/${subject.courseId}`)
			.then(res => res.json())
			.then(data => {
				document.querySelector('.courseDetails').insertAdjacentHTML('afterbegin',
					`
					<tr>
						<td> ${data.name}</td>
						<td> ${data.description}</td>
						<td> ${subject.enrolledOn}</td>
						<td> ${subject.status}</td>
					</tr>	
					`
				)
			})
		})

		// ======================================================================

		userProfile.innerHTML = `
			<div class="col-md-13">
				<section class="jumbotron my-5">
					<h3 class="text-center">First Name: ${data.firstName}</h3>
					<h3 class="text-center">Last Name: ${data.lastName}</h3>
					<h3 class="text-center">Email: ${data.email}</h3>
					<h3 class="text-center">Mobile Number: ${data.mobileNo}</h3>
					<table class='table'>
						<tr>
							<th>Course Name: </th>
							<th>Course Description: </th>						
							<th>Enrolled On: </th>
							<th>Status: </th>
							<tbody class="courseDetails"></tbody>
						</tr>
					</table>


				</section>
			</div>
		`
	})
