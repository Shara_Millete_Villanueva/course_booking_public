// create add course page
let formSubmit = document.querySelector('#createCourse');

//put an event that will be applied to the form component
// create a subfunction inside the method to describe the action when user triggers the event  

formSubmit.addEventListener("submit", (event) => {
	event.preventDefault()

	//target values of each component inside the forms / values in input fields
	let courseName = document.querySelector("#courseName").value;
	let coursePrice = document.querySelector("#coursePrice").value;
	let courseDesc = document.querySelector("#courseDesc").value;

	//check to see if we were able to capture the values of input fields
	// console.log(courseName);
	// console.log(coursePrice);
	// console.log(courseDesc);

// ----------------------
const isAdmin = localStorage.getItem("isAdmin")
//create a controll structure to determine the display in the front end
if (isAdmin == "false" || !isAdmin) {
	fetch("https://protected-garden-34848.herokuapp.com/api/login")
		.then(res => res.json())
		.then(data => data.send);
} else {

	if (courseName !== "" && coursePrice !== "" && courseDesc !== "" ) {
		//send a fetch request to courseExists backend to check if course already exists
		fetch ("https://protected-garden-34848.herokuapp.com/api/courses", {
			method: 'POST' ,
			headers: {
				'content-type': 'Application/json'
			},
			body: JSON.stringify({
				name: courseName
			})
		}).then(res => {
			return res.json()
		}).then(data => {
			if (data === true) {
				// alert('Course already exists!')
				Swal.fire ({
				  icon: 'error',
				  text: 'This course is already in the system!',
				  footer: '<a href>Ok, ill try another one</a>'
				})
			} else {
				// send request to back-end to process the data for saving/creating a new entry inside the courses collection in the database.
				fetch('https://protected-garden-34848.herokuapp.com/api/courses/create', {
					method: 'POST',
					headers: {
						'content-type': 'Application/json'
					},
					body: JSON.stringify({
						name: courseName,
						description: courseDesc,
						price: coursePrice
					})
				}).then(res => {
					return res.json()
					// console.log(res)
				}).then(data => {
					// console.log(data)
					if (data === true) {
						Swal.fire ({
						  icon: 'success',
						  title: 'New course has been saved!',
						  text: 'Congratulations!',
						  footer: '<a href>Great!</a>'
						})
					} 
					else {
						Swal.fire({
						  icon: 'error',
						  title: 'Oops...',
						  text: 'Something went wrong!',
						  footer: '<a href>Ok, let me try again</a>'
						})
					}
				})
			}
		})
		} else {
			Swal.fire({
			  icon: 'error',
			  title: 'Oops...',
			  text: 'Something went wrong!',
			  footer: '<a href>Ok, let me try again</a>'
			})
		}
	}

})


// ----------------------


